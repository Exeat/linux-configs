# Lines configured by zsh-newuser-install
HISTFILE=~/.cache/zsh/history
HISTSIZE=100000
SAVEHIST=100000
setopt autocd
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
export EDITOR=vim

#Reads a file to set the colors
for f in ~/.config/shell/color.conf; do source "$f"; done

#Alia file so I don't need to update for more then 1 shell
for f in ~/.config/shell/alias; do source "$f"; done

# Check if the session is an SSH login
if [[ -n "$SSH_CONNECTION" ]]; then
    # comand to run on ssh login
    nf
    # Any other commands you want to run on SSH login
fi


##Un coament if you aren't me and just want to set the color
#namecolor=3m
#hostcolor=21m
#atcolor=red
boardercolor=gray
dircolor=blue


autoload -Uz compinit
compinit
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)

#added paths
PATH=$PATH:~/.local/bin

autoload -U colors && colors

# Uses the vars at the top of the file change them there if you don't like how it is
PS1="%B%F{$boardercolor}[%F{$namecolor}%n$atcolor@%F{$hostcolor}%M %F{$dircolor}%~%F{$boardercolor}]%f%b$ "



