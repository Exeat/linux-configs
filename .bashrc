# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#         Border             name                 @                 host                 dir                Border
PS1="\[\e[99m\][\[\e[m\]\[\e[33m\]\u\[\e[m\]\[\e[31m\]@\[\e[m\]\[\e[32m\]\h\[\e[m\]:\[\e[36m\]\w\[\e[m\]\[\e[99m\]]\[\e[m\]\[\e[99m\]\\$\[\e[m\] "

export EDITOR=vim

#Alias
for f in ~/alias; do source "$f"; done

PATH=$PATH:~/.scripts

