# Setup
export XDG_CONFIG_HOME="${HOME}/.config"

export XDG_CACHE_HOME="${HOME}/.cache"

export XDG_DATA_HOME="${HOME}/.local/share"

export XDG_DATA_DIRS="/usr/local/share:/usr/share"

export XDG_CONFIG_DIRS="/etc/xdg"

export XDG_STATE_HOME=$HOME/.local/state


export ANDROID_HOME="$XDG_DATA_HOME"/android
export MINETEST_USER_PATH="$XDG_DATA_HOME"/minetest
export ZDOTDIR="$HOME"/.config/shell
export WINEPREFIX="$XDG_DATA_HOME"/wine
export ANDROID_HOME="$XDG_DATA_HOME"/android
export HISTFILE="${XDG_STATE_HOME}"/bash/history
export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
export LESSHISTFILE="$XDG_STATE_HOME"/less/history
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export TS3_CONFIG_DIR=$XDG_CONFIG_HOME/ts3client
export _JAVA_OPTIONS="-Djava.util.prefs.userRoot=${XDG_CONFIG_HOME}/java -Djavafx.cachedir=${XDG_CACHE_HOME}/openjfx"
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export KODI_DATA="$XDG_DATA_HOME"/kodi

alias nvidia-settings="nvidia-settings --config="$XDG_CONFIG_HOME"/nvidia/settings"

#alias fixes
alias xbindkeys=xbindkeys -f "$XDG_CONFIG_HOME"/xbindkeysrc
alias wget=wget --hsts-file="$XDG_DATA_HOME"wget-hsts
alias monerod=monerod --data-dir "$XDG_DATA_HOME"/bitmonero
