# Setup
export XDG_CONFIG_HOME="${HOME}/.config"

export XDG_CACHE_HOME="${HOME}/.cache"

export XDG_DATA_HOME="${HOME}/.local/share"

export XDG_DATA_DIRS="/usr/local/share:/usr/share"

export XDG_CONFIG_DIRS="/etc/xdg"

# programs

#wine
export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
#wget
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
#NOTE add alias wget --hsts-file="$XDG_CACHE_HOME/wget-hsts" to RC file
#Elinks
export ELINKS_CONFDIR="$XDG_CONFIG_HOME"/elinks
#zsh
export HISTFILE="$XDG_DATA_HOME"/zsh/history
